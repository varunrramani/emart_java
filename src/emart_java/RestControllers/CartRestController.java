/**
 * REST service for Deleting and Updating cart items
 */
package emart_java.RestControllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

import emart_java.DAOInterfaces.CartDAO;
import emart_java.Entities.CartTable;
import emart_java.Entities.UserInfo;

/**
 * @author Varun Ramani
 *
 */

@RestController
public class CartRestController {

	@Autowired
	CartDAO cartDao;

	@RequestMapping(method = RequestMethod.GET, value = "/cart/{userId}", headers = "Accept=application/json")
	public List<CartTable> getCartItems(@PathVariable("userId") int userId, HttpSession httpSession) {
		List<CartTable> cartItems = cartDao.getCartItems(getUserInfoFromSession(httpSession));
		// System.out.println(cartItems);
		Gson gson = new Gson();
		String listCartTable = gson.toJson(cartItems);
		System.out.println(listCartTable);

		return cartItems;
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/cart/{cartId}", headers = "Accept=application/json")
	public String deleteCartItem(@PathVariable("cartId") int cartId, HttpSession httpSession) {
		cartDao.deleteCartItem(cartId);

		List<CartTable> cartItems = cartDao.getCartItems(getUserInfoFromSession(httpSession));
		// System.out.println(cartItems);
		Gson gson = new Gson();
		String listCartTable = gson.toJson(cartItems);
		System.out.println(listCartTable);

		return listCartTable;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/cart/{cartId}/{quantity}", headers = "Accept=application/json")
	public String updateCartItem(@PathVariable("cartId") int cartId, @PathVariable("quantity") int quantity,
			HttpSession httpSession) {

		cartDao.updateCartItem(cartId, quantity);

		List<CartTable> cartItems = cartDao.getCartItems(getUserInfoFromSession(httpSession));
		Gson gson = new Gson();
		String listCartTable = gson.toJson(cartItems);
		System.out.println(listCartTable);

		return listCartTable;
	}

	private UserInfo getUserInfoFromSession(HttpSession httpSession) {
		UserInfo userInfo = new UserInfo();
		// userInfo.setUserId(Integer.parseInt(httpSession.getAttribute("userId").toString()));
		userInfo.setUserId(1);
		return userInfo;
	}
}
