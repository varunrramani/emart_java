/**
 * 
 */
package emart_java.Entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "UserInfo")
public class UserInfo {

	private int userId;
	private String name;
	private String emailId;
	private String password;
	private Date birthDate;
	private int mobileNo;
	private String educationalQualification;
	private String occupation;
	private String gender;
	private double emCardPoints;
	private String isEmCardHolder;
	private AddressTable addressTable;
	
	public UserInfo() {
	}

	public UserInfo(int userId, String name, String emailId, Date birthDate, int mobileNo,
			String educationalQualification, String occupation, String gender, double emCardPoints,
			String isEmCardHolder, AddressTable addressTable) {
		this.userId = userId;
		this.name = name;
		this.emailId = emailId;
		this.birthDate = birthDate;
		this.mobileNo = mobileNo;
		this.educationalQualification = educationalQualification;
		this.occupation = occupation;
		this.gender = gender;
		this.emCardPoints = emCardPoints;
		this.isEmCardHolder = isEmCardHolder;
		this.addressTable = addressTable;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public int getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(int mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEducationalQualification() {
		return educationalQualification;
	}

	public void setEducationalQualification(String educationalQualification) {
		this.educationalQualification = educationalQualification;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getEmCardPoints() {
		return emCardPoints;
	}

	public void setEmCardPoints(double emCardPoints) {
		this.emCardPoints = emCardPoints;
	}

	@OneToOne(cascade = CascadeType.ALL)
	public AddressTable getAddTable() {
		return addressTable;
	}

	public void setAddTable(AddressTable addTable) {
		this.addressTable = addTable;
	}

	@Column(nullable = true)
	public String getIsEmCardHolder() {
		return isEmCardHolder;
	}

	public void setIsEmCardHolder(String isEmCardHolder) {
		this.isEmCardHolder = isEmCardHolder;
	}

}
