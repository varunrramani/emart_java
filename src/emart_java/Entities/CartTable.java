package emart_java.Entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class CartTable {
	private int cartId;
	private int quantity;
	private boolean active;
	private boolean emCurrencyPointsCombination;
	private UserInfo userInfo;
	private ProductTable productTable;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isEmCurrencyPointsCombination() {
		return emCurrencyPointsCombination;
	}

	public void setEmCurrencyPointsCombination(boolean emCurrencyPointsCombination) {
		this.emCurrencyPointsCombination = emCurrencyPointsCombination;
	}

	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo uInfo) {
		this.userInfo = uInfo;
	}

	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	public ProductTable getProductTable() {
		return productTable;
	}

	public void setProductTable(ProductTable pTable) {
		this.productTable = pTable;
	}

	@Override
	public String toString() {
		return "CartTable [cartId=" + cartId + ", quantity=" + quantity + ", active=" + active
				+ ", emCurrencyPointsCombination=" + emCurrencyPointsCombination + ", userInfo=" + userInfo
				+ ", productTable=" + productTable + "]";
	}

}
