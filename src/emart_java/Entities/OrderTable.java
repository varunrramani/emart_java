package emart_java.Entities;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class OrderTable {
	private int orderId;
	private int orderNumber;
	private double orderTotal;
	private Date orderDate;
	private List<OrderItemsTable> orderItemsTable;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getOrderId() {
		return orderId;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public double getOrderTotal() {
		return orderTotal;
	}

	public void setOrderTotal(double orderTotal) {
		this.orderTotal = orderTotal;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	public List<OrderItemsTable> getOrderItemsTable() {
		return orderItemsTable;
	}

	public void setOrderItemsTable(List<OrderItemsTable> oItemTable) {
		this.orderItemsTable = oItemTable;
	}

	@Override
	public String toString() {
		return "OrderTable [orderId=" + orderId + ", orderNumber=" + orderNumber + ", orderTotal=" + orderTotal
				+ ", orderDate=" + orderDate + ", orderItemTable=" + orderItemsTable + "]";
	}

}
