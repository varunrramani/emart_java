package emart_java.Entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Order_Items_Table")
public class OrderItemsTable {
	private long itemId;
	private int quantity;
	private double costPrice;
	private double sellingPrice;
	private double emCardHoldersPrice;
	private int emCardHoldersPoints;
	private ProductTable productTable;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getItemId() {
		return itemId;
	}

	public void setItemId(long itemId) {
		this.itemId = itemId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public double getEmCardHoldersPrice() {
		return emCardHoldersPrice;
	}

	public void setEmCardHoldersPrice(double emCardHoldersPrice) {
		this.emCardHoldersPrice = emCardHoldersPrice;
	}

	public int getEmCardHoldersPoints() {
		return emCardHoldersPoints;
	}

	public void setEmCardHoldersPoints(int emCardHoldersPoints) {
		this.emCardHoldersPoints = emCardHoldersPoints;
	}

	@OneToOne(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	public ProductTable getProductTable() {
		return productTable;
	}

	public void setProductTable(ProductTable productTable) {
		this.productTable = productTable;
	}

	@Override
	public String toString() {
		return "OrderItemsTable [itemId=" + itemId + ", quantity=" + quantity + ", costPrice=" + costPrice
				+ ", sellingPrice=" + sellingPrice + ", emCardHoldersPrice=" + emCardHoldersPrice
				+ ", emCardHoldersPoints=" + emCardHoldersPoints + ", productTable=" + productTable + "]";
	}

}
