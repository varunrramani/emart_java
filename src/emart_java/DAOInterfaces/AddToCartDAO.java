package emart_java.DAOInterfaces;
 
import javax.servlet.http.HttpSession;

import emart_java.Entities.UserInfo;

 
public interface AddToCartDAO {
	String addToCart(int productId, UserInfo user, boolean emCurrencyCardCombination, HttpSession session);
}
