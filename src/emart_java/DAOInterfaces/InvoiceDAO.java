/**
 * 
 */
package emart_java.DAOInterfaces;

import emart_java.Entities.OrderTable;

/**
 * @author Varun Ramani
 *
 */
public interface InvoiceDAO {
	Integer createOrder(int userId);
	
	OrderTable getOrderDetails(int orderId);
}
