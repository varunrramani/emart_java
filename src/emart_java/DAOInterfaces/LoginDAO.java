package emart_java.DAOInterfaces;

 
import java.util.List;

import emart_java.Entities.AddressTable;
import emart_java.Entities.UserInfo;

public interface LoginDAO {
	
	List verifyLogin(UserInfo ref);
	boolean verifyRegistration(UserInfo ref);
	void saveAddress(AddressTable ref);
}
