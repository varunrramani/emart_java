package emart_java.DAOInterfaces;

import java.util.List;
 
 
public interface CategoryDAO {

	boolean checkHasSubCategory(int id);

	List fetchAllCategory();

	List fetchAllSubCategory(int id);
}
