package emart_java.DAOInterfaces;

import java.util.List;
import emart_java.Entities.CartTable;
import emart_java.Entities.UserInfo;

public interface CartDAO {
	List<CartTable> getCartItems(UserInfo userInfo);

	Boolean updateCartItem(Integer cartId, Integer quantity);
	
	Boolean deleteCartItem(Integer cartId);
}
