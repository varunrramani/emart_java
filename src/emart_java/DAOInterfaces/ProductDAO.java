package emart_java.DAOInterfaces;

import java.util.List;

import emart_java.Entities.ProductTable;

public interface ProductDAO {

	//public List fetch();

	//public List fetch(ProductTable pt);

	public List<ProductTable> getDetails(int id);

}
