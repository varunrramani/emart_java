/**
 * Controller to return CartItems to the Cart View
 */
package emart_java.Controllers;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import emart_java.DAOInterfaces.CartDAO;
import emart_java.Entities.CartTable;
import emart_java.Entities.UserInfo;

/**
 * @author Varun Ramani
 *
 */
@Controller
public class CartController {

	@Autowired
	CartDAO cartDao;

	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public ModelAndView getCartItems(HttpServletRequest request, HttpSession httpSession) {
		// httpSession.setAttribute("userId", 1);
		UserInfo userInfo = (UserInfo) httpSession.getAttribute("user");
		// userInfo.setUserId(Integer.parseInt(httpSession.getAttribute("userId").toString()));
		// userInfo.setUserId(1);
		List<CartTable> cartItems = cartDao.getCartItems(userInfo);
		// System.out.println(cartItems);
		Gson gson = new Gson();
		String listCartTable = gson.toJson(cartItems);
		System.out.println(listCartTable);

		// System.out.println("EM" + new BigInteger(32, new
		// SecureRandom()).toString());

		// TODO: send listCartTable in ModelAndView
		return new ModelAndView("cart.definition", "listCartTable", cartItems);
	}

	@RequestMapping(value = "deleteCartItem", method = RequestMethod.POST)
	public ModelAndView deleteCartItem(HttpServletRequest request, HttpSession httpSession) {

		Integer cartId = Integer.parseInt(request.getParameter("cartId"));

		cartDao.deleteCartItem(cartId);

		UserInfo userInfo = (UserInfo) httpSession.getAttribute("user");
		// userInfo.setUserId(Integer.parseInt(httpSession.getAttribute("user").toString()));
		List<CartTable> cartItems = cartDao.getCartItems(userInfo);
		// System.out.println(cartItems);
		Gson gson = new Gson();
		String listCartTable = gson.toJson(cartItems);
		System.out.println(listCartTable);
		return new ModelAndView("cart.definition", "listCartTable", cartItems);
	}

	@RequestMapping(value = "updateCartItem", method = RequestMethod.POST)
	public ModelAndView updateCartItem(HttpServletRequest request, HttpSession httpSession) {

		Integer cartId = Integer.parseInt(request.getParameter("cartId").trim());
		Integer quantity = Integer.parseInt(request.getParameter("quantity").trim());
		cartDao.updateCartItem(cartId, quantity);
		UserInfo userInfo = (UserInfo) httpSession.getAttribute("user");
		List<CartTable> cartItems = cartDao.getCartItems(userInfo);
		// System.out.println(cartItems);
		Gson gson = new Gson();
		String listCartTable = gson.toJson(cartItems);
		System.out.println(listCartTable);
		return new ModelAndView("cart.definition", "listCartTable", cartItems);
	}
}
