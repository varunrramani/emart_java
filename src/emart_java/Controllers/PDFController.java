/**
 * Controller to getInvoice and generate PDF for it
 */
package emart_java.Controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.InvoiceDAO;
import emart_java.Entities.OrderTable;

/**
 * @author Varun Ramani
 *
 */
@Controller
public class PDFController {

	@Autowired
	InvoiceDAO invoiceDao;

	@RequestMapping(value = "generate", method = RequestMethod.GET)
	public ModelAndView generatePdf(HttpServletRequest request, HttpSession session) {
		int orderId = Integer.parseInt(request.getParameter("orderId").toString().trim());
		OrderTable order = invoiceDao.getOrderDetails(orderId);
		return new ModelAndView("pdfView", "order", order);
	}
}
