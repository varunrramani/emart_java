/**
 * Generate PDF of the invoice
 */
package emart_java.Controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import emart_java.Entities.OrderItemsTable;
import emart_java.Entities.OrderTable;
import emart_java.Utils.AbstractITextPdfView;

/**
 * @author Varun Ramani
 * 
 */
public class PDFView extends AbstractITextPdfView {

	@Override
	protected void buildPdfDocument(Map<String, Object> map, Document document, PdfWriter pdfWriter,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		OrderTable order = (OrderTable) map.get("order");
		document.addTitle("Your Invoice");
		document.add(new Paragraph("Your Invoice"));

		document.add(new Paragraph("Order Number: " + order.getOrderNumber()));
		document.add(new Paragraph("Order Date: " + order.getOrderDate().toString()));
		document.add(new Paragraph("Order Total: " + String.valueOf(order.getOrderTotal())));

		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(BaseColor.BLACK);

		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(BaseColor.BLUE);
		cell.setPadding(5);

		cell.setPhrase(new Phrase("Sr."));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Name"));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Quantity"));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Selling Price"));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Card Holder Price"));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Card Holder Point"));
		table.addCell(cell);

		cell.setPhrase(new Phrase("Amount"));
		table.addCell(cell);

		int i = 1;

		List<OrderItemsTable> items = order.getOrderItemsTable();

		for (OrderItemsTable item : items) {
			table.addCell(String.valueOf(i++));
			table.addCell(item.getProductTable().getProductName());
			table.addCell(String.valueOf(item.getQuantity()));
			table.addCell(String.valueOf(item.getSellingPrice()));
			table.addCell(String.valueOf(item.getEmCardHoldersPrice()));
			table.addCell(String.valueOf(item.getEmCardHoldersPoints()));
			table.addCell(String.valueOf(item.getQuantity() * item.getSellingPrice()));
		}

		document.add(table);
	}

}
