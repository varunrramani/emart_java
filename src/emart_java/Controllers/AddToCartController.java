package emart_java.Controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.AddToCartDAO;
import emart_java.Entities.UserInfo;

@Controller
@RequestMapping("/addToCart")
public class AddToCartController {

	@Autowired
	AddToCartDAO cartDao;
	boolean emCurrencyCardCombination;
	
	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody void addProductDetailsToCart(HttpServletRequest request, HttpServletResponse response, HttpSession session)
	{
		//System.out.println(request.getParameterNames().toString());
		int productID=Integer.parseInt(request.getParameter("productId").trim());
		if(request.getParameter("chk")!=null)
		{
			emCurrencyCardCombination=Boolean.parseBoolean(request.getParameter("chk"));
		}
		UserInfo user= (UserInfo) session.getAttribute("user");
		if(user==null)
		{
			//return new ModelAndView("login.definition");
			String message = "Login";
			try {
				response.getWriter().write(message);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		String message = cartDao.addToCart(productID,user,emCurrencyCardCombination,session);
//		return new ModelAndView("productList.definition");
		try {
			response.getWriter().write(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
