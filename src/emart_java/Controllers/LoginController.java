package emart_java.Controllers;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.LoginDAO;
import emart_java.Entities.AddressTable;
import emart_java.Entities.UserInfo;

@Controller 
public class LoginController
{
	@Autowired
	LoginDAO logindao;
	
	@RequestMapping(value="/login" ,method = RequestMethod.GET)
	public ModelAndView before()
	{
		UserInfo obj=new UserInfo();
		return new ModelAndView("login.definition","Login",obj);
	}
	
	@RequestMapping(value="/login",method = RequestMethod.POST)
	public ModelAndView After(UserInfo login,HttpSession session)
	{ 
		List<UserInfo> list=logindao.verifyLogin(login);
		if(list!= null)
		{
			session.setAttribute("user",list.get(0));
			session.setAttribute("cardHolder", list.get(0).getIsEmCardHolder());
			return new ModelAndView("redirect:/index.jsp");
		}
		else
			return new ModelAndView("redirect:/login.do");
	}
	
	@RequestMapping(value="/registration",method = RequestMethod.GET)
	public ModelAndView Regbefore()
	{
		UserInfo userinfo = new UserInfo();
		return new ModelAndView("registration.definition","userinfo",userinfo);
	}
	
	@RequestMapping(value="/registration",method = RequestMethod.POST)
	public ModelAndView RegAfter(UserInfo userinfo)
	{ 
		if(logindao.verifyRegistration(userinfo))
		{
			return new ModelAndView("redirect:/address.do?id="+userinfo.getUserId());
		}	
		else
			return new ModelAndView("redirect:/login.do");
	}
	
	@RequestMapping(value="/address",method = RequestMethod.GET)
	public ModelAndView AddressBefore(HttpServletRequest request)
	{ 
	//int id=	Integer.parseInt(request.getParameter("id"));
	
		    AddressTable address = new AddressTable();
			return new ModelAndView("address.definition","address",address);
	}
	
	@RequestMapping(value="/address",method = RequestMethod.POST)
	public ModelAndView AddressAfter(AddressTable address)
	{ 
		logindao.saveAddress(address);
		return new ModelAndView("redirect:/index.jsp");
	}
}