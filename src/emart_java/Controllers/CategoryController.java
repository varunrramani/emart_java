package emart_java.Controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.util.SystemPropertyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.CategoryDAO;
import emart_java.Entities.CategoryTable;

@Controller
public class CategoryController {

	@Autowired
	CategoryDAO categoryDAO;

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public ModelAndView displayMainCategory() {
		List<CategoryTable> categoryList = categoryDAO.fetchAllCategory();
		return new ModelAndView("category.definition", "CategoryList", categoryList);
	}

	@RequestMapping(value = "/SubCategory", method = RequestMethod.GET)
	public ModelAndView displaySubCategory(HttpServletRequest request) {
		int id = Integer.parseInt(request.getParameter("id").trim());
		System.out.println(id);
		List<Object> subCategoryList = categoryDAO.fetchAllSubCategory(id);
		if (subCategoryList.get(0) instanceof CategoryTable) {
			return new ModelAndView("subCategory.definition", "SubCategoryList", subCategoryList);
		} else {
			return new ModelAndView("productList.definition", "ProductList", subCategoryList);
		}
	}
}
