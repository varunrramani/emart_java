/**
 * Generate invoice on click of Pay Now on Cart
 */
package emart_java.Controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import emart_java.DAOInterfaces.InvoiceDAO;
import emart_java.Entities.OrderItemsTable;
import emart_java.Entities.OrderTable;
import emart_java.Entities.UserInfo;

/**
 * @author Varun Ramani
 *
 */

@Controller
public class InvoiceController {
	@Autowired
	InvoiceDAO invoiceDao;

	@RequestMapping(value = "createOrder", method = RequestMethod.POST)
	public String createOrder(HttpSession session, RedirectAttributes redirectAttributes) {

		UserInfo userInfo = (UserInfo) session.getAttribute("user");
		try {
			int orderId = invoiceDao.createOrder(userInfo.getUserId());

			redirectAttributes.addFlashAttribute("orderId", orderId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:getInvoice.do";
	}

	@RequestMapping(value = "getInvoice", method = RequestMethod.GET)
	public ModelAndView getInvoice(HttpSession session, @ModelAttribute("orderId") int orderId) {

		// UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
		OrderTable order = invoiceDao.getOrderDetails(orderId);
		System.out.println(order);

		return new ModelAndView("invoice.definition", "order", order);
	}
}