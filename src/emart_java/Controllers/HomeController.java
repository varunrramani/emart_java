package emart_java.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.CategoryDAO;
import emart_java.Entities.CategoryTable;

@Controller
@RequestMapping("/home")
public class HomeController {
	@Autowired
	CategoryDAO categoryDAO;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView before() {
		List<CategoryTable> categoryList = categoryDAO.fetchAllCategory();
		ModelAndView modelAndView = new ModelAndView("base.definition");
		modelAndView.addObject("CategoryList", categoryList);
		return modelAndView;
	}

}
