package emart_java.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.ProductAdminDAO;
import emart_java.Entities.ProductTable;

@Controller
@RequestMapping("/new_Product")
public class ProductAdmin {

	@Autowired
	ProductAdminDAO pdao;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView before() {
		ProductTable productTable = new ProductTable();
		// return new ModelAndView("product.definition","product",productobj);
		return new ModelAndView("Product", "productInfo", productTable);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processSubmit(@Valid @ModelAttribute("productInfo") ProductTable productTable,
			BindingResult result) {
		// sdao.ad3d1(productobj1);
		pdao.add(productTable);
		// Give link to go to add in price table
		productTable = new ProductTable();
		// return new ModelAndView("product.definition","product",productobj);
		return new ModelAndView("Product", "productInfo", productTable);

	}
}