package emart_java.Controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import emart_java.DAOInterfaces.ProductDAO;
import emart_java.Entities.ProductTable;

@Controller
public class PTableController  {

	
	
	@Autowired
	ProductDAO pdao;
	
	
	@RequestMapping(value="/productDetails",method=RequestMethod.GET)
	public ModelAndView Dispaly(HttpServletRequest request)
	{
		int id=Integer.parseInt(request.getParameter("id"));
		List productList=  pdao.getDetails(id);
		return new ModelAndView("ProductFullDetails","productDisplay",productList) ;
		
	}
	
	
	
}
