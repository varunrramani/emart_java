package emart_java.DAOImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.DAOInterfaces.ProductAdminDAO;
import emart_java.Entities.ProductTable;

@Component
public class ProductAdminDAOImpl implements ProductAdminDAO {

	@Autowired
	private HibernateTemplate template;

	@Override
	public void add(ProductTable productTable) {
		try {
			template.save(productTable);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
