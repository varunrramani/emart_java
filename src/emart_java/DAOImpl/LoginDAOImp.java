package emart_java.DAOImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.DAOInterfaces.LoginDAO;
import emart_java.Entities.AddressTable;
import emart_java.Entities.CartTable;
import emart_java.Entities.UserInfo;

@Component
public class LoginDAOImp implements LoginDAO{

	@Autowired
	private HibernateTemplate template;
	
	public List<UserInfo> verifyLogin(UserInfo ref) 
	{
		String email=ref.getEmailId();
		String pass=ref.getPassword();
		List<UserInfo> list=(List<UserInfo>) template.find("from UserInfo u where u.emailId='"+email+"' AND u.password='"+pass+"'");
		return list;
	}

	@Override
	public boolean verifyRegistration(UserInfo ref) {
		Object r =template.save(ref);
		if(r==null)
		{
			return false;
		}
		else
		return true;
	}

	@Override
	public void saveAddress(AddressTable ref) {
		 template.save(ref);
	}
}