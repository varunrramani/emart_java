/**
 * DAO implementation for creating order and generating invoice
 */
package emart_java.DAOImpl;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.DAOInterfaces.CartDAO;
import emart_java.DAOInterfaces.InvoiceDAO;
import emart_java.Entities.CartTable;
import emart_java.Entities.OrderItemsTable;
import emart_java.Entities.OrderTable;
import emart_java.Entities.UserInfo;

/**
 * @author Varun Ramani
 *
 */
@Component
public class InvoiceDAOImpl implements InvoiceDAO {

	@Autowired
	HibernateTemplate template;

	@Autowired
	CartDAO cartDao;

	@Override
	public Integer createOrder(int userId) {
		// TODO Auto-generated method stub
		UserInfo userInfo = new UserInfo();
		userInfo.setUserId(userId);
		List<CartTable> cartItems = cartDao.getCartItems(userInfo);

		List<OrderItemsTable> orderItems = new ArrayList<>();
		List<UserInfo> userInfoList = (List<UserInfo>) template.find("from UserInfo as u where u.userId = " + userId);
		userInfo = userInfoList.get(0);
		String isEmCardHolder = userInfo.getIsEmCardHolder();
		int orderTotal = 0;
		int pointsTotal = 0;
		for (CartTable cartTable : cartItems) {
			OrderItemsTable orderItemsTable = new OrderItemsTable();
			orderItemsTable.setQuantity(cartTable.getQuantity());
			orderItemsTable.setProductTable(cartTable.getProductTable());
			orderItemsTable.setCostPrice(cartTable.getProductTable().getPriceTable().getCostPrice());
			orderItemsTable.setSellingPrice(cartTable.getProductTable().getPriceTable().getSellingPrice());
			orderItemsTable.setEmCardHoldersPrice(cartTable.getProductTable().getPriceTable().getEmCardHoldersPrice());
			orderItemsTable.setEmCardHoldersPoints(cartTable.getProductTable().getPriceTable().getEmCardPoints());
			if (isEmCardHolder.equalsIgnoreCase("yes")) {
				orderTotal += cartTable.getQuantity()
						* cartTable.getProductTable().getPriceTable().getEmCardHoldersPrice();
			} else {
				orderTotal += cartTable.getQuantity() * cartTable.getProductTable().getPriceTable().getSellingPrice();
			}
			/*
			 * if (isCheckBox for points checked) { pointsTotal +=
			 * cartTable.getQuantity() *
			 * cartTable.getpTable().getPriceTable().getEmCardPoints(); }
			 */
			orderItems.add(orderItemsTable);
		}
		OrderTable orderTable = new OrderTable();

		orderTable.setOrderItemsTable(orderItems);
		orderTable.setOrderDate(new Date(new java.util.Date().getDate(), new java.util.Date().getMonth(),
				new java.util.Date().getYear()));

		orderTable.setOrderNumber(getOrderNumber());

		template.save(orderTable);
		setCartItemsInactive(cartItems);
		int pointsToBeDeducted = pointsTotal; // TODO: sum of points for which
												// check box is checked
		int pointsToBeGiven = orderTotal / 10;
		userInfo.setEmCardPoints(userInfo.getEmCardPoints() - pointsToBeDeducted + pointsToBeGiven);
		template.saveOrUpdate(userInfo);

		return orderTable.getOrderId();
	}

	private void setCartItemsInactive(List<CartTable> cartItems) {
		for (CartTable cartTable : cartItems) {
			cartTable.setActive(false);
			template.saveOrUpdate(cartTable);
		}
	}

	String orderTableName = OrderTable.class.getSimpleName();

	@Override
	public OrderTable getOrderDetails(int orderId) {
		List<OrderTable> orderItems = (List<OrderTable>) template
				.find("From " + orderTableName + " as o where o.orderId = " + orderId);
		return orderItems.get(0);
	}

	private Integer getOrderNumber() {
		SecureRandom random = new SecureRandom();
		return Integer.parseInt(new BigInteger(24, random).toString());
	}

}
