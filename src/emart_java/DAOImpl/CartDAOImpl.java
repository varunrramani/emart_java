/**
 * DAO implementation for fetching Cart details for a particular user
 */
package emart_java.DAOImpl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.HibernateUtil;
import emart_java.DAOInterfaces.CartDAO;
import emart_java.Entities.CartTable;
import emart_java.Entities.ProductTable;
import emart_java.Entities.UserInfo;

/**
 * @author Varun Ramani
 *
 */
@Component
public class CartDAOImpl implements CartDAO {

	@Autowired
	HibernateTemplate hibernateTemplate;

	private String tableName = CartTable.class.getSimpleName();
	/*
	 * UserInfo userInfo = new UserInfo(1, "Varun", "varun@varun.com", new
	 * Date(14, 05, 1991), 1234567890, "ABC", "ABC", "Male", 100, true, null);
	 * 
	 * ProductTable productTable = new ProductTable(null, "ABC", 1, 1, 0, "",
	 * ""); CartTable cartTable = new CartTable(1, 1, 1, userInfo,
	 * productTable);
	 */

	@Override
	public List<CartTable> getCartItems(UserInfo userInfo) {
		/*
		 * List<CartTable> cartItems = (List<CartTable>)
		 * hibernateTemplate.find("from " + tableName +
		 * " where userInfo.userId = ?", userInfo.getUserId());
		 */
		// String hql = "from " + tableName + " where userInfo.userId =
		// :userId";
		// Query query =
		// HibernateUtil.getSessionFactory().getCurrentSession().createQuery(hql);
		/*
		 * Query query =
		 * HibernateUtil.getSessionFactory().getCurrentSession().getNamedQuery(
		 * "getCartItems") .setString("userId",
		 * String.valueOf(userInfo.getUserId())); List<CartTable> cartItems =
		 * query.list();
		 */
		/*
		 * List<CartTable> cartItems = (List<CartTable>)
		 * hibernateTemplate.findByNamedQueryAndNamedParam("getCartItems",
		 * "userId", userInfo.getUserId());
		 */
		List<CartTable> cartItems = (List<CartTable>) hibernateTemplate
				.find("from CartTable c where c.userInfo.userId = " + userInfo.getUserId() + " and c.active=true");// ,
																							// "userId",
																							// userInfo.getUserId());

		/*
		 * List<CartTable> cartItems = new ArrayList<>();
		 * 
		 * cartItems.add(cartTable); cartItems.add(cartTable);
		 * cartItems.add(cartTable); cartItems.add(cartTable);
		 */
		return cartItems;
	}

	@Override
	public Boolean updateCartItem(Integer cartId, Integer quantity) {
		Boolean updated = true;
		/*
		 * String hql = "UPDATE " + tableName +
		 * " set quantity = :quantity WHERE cartId = :cartId"; Query query =
		 * HibernateUtil.getSessionFactory().getCurrentSession().createQuery(hql
		 * ); query.setParameter("quantity", quantity);
		 * query.setParameter("cartId", cartId);
		 */

		/*
		 * Query query =
		 * HibernateUtil.getSessionFactory().getCurrentSession().getNamedQuery(
		 * "updateCartItem") .setInteger("cartId",
		 * cartId).setInteger("quantity", quantity); int result =
		 * query.executeUpdate(); if (result == 1) { updated = true; }
		 */

		/*
		 * Session session =
		 * hibernateTemplate.getSessionFactory().getCurrentSession();
		 * session.update("updateCartItem", new Object[] { cartId, quantity });
		 */
		hibernateTemplate.bulkUpdate(
				"UPDATE " + tableName + " as c set c.quantity = " + quantity + " WHERE c.cartId = " + cartId);
		return updated;
	}

	@Override
	public Boolean deleteCartItem(Integer cartId) {
		Boolean deleted = false;

		/*
		 * String hql = "DELETE FROM " + tableName + " WHERE cartId = :cartId";
		 * Query query =
		 * HibernateUtil.getSessionFactory().getCurrentSession().createQuery(hql
		 * ); query.setParameter("cartId", cartId); int result =
		 * query.executeUpdate();
		 * 
		 * if (result == 1) { deleted = true; }
		 */

		/*
		 * Session session =
		 * hibernateTemplate.getSessionFactory().getCurrentSession();
		 * session.delete("deleteCartItem", new Object[] { cartId });
		 */

		hibernateTemplate.bulkUpdate("UPDATE " + tableName + " as c set c.active=false where c.cartId = " + cartId);

		return deleted;
	}

}
