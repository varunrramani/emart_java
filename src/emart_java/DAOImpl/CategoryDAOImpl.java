package emart_java.DAOImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.DAOInterfaces.CategoryDAO;
import emart_java.Entities.CategoryTable;
import emart_java.Entities.ProductTable;

@Component
public class CategoryDAOImpl implements CategoryDAO {

	@Autowired
	private HibernateTemplate template;
	
	@Override
	public List fetchAllCategory() {
		/*CategoryTable category1=new CategoryTable(null,"Fridge",true,"fridge.jpg",true);
		CategoryTable category2=new CategoryTable(category1,"LG",false,"Lg.jpg",false);
		//@SuppressWarnings("unchecked")
		List<CategoryTable> mylist=new ArrayList<CategoryTable>();
		mylist.add(category1);
		mylist.add(category2);*/
		@SuppressWarnings("unchecked")
		/*if(checkHasMainCategory(id))
		{*/
		List<CategoryTable> mylist = (List<CategoryTable>) template.find("from CategoryTable c where c.isMainCategory=true");
		return mylist;
		/*}
	}
	@Override
	public boolean checkHasMainCategory(int id) {
		List category=template.find("select c.hasMainCategory from CategoryTable c where c.categoryId="+id);
		return (boolean)category.get(0); 
	}*/
	}

	@Override
	public boolean checkHasSubCategory(int id) {
		List subcategory=template.find("select c.hasSubCategory from CategoryTable c where c.categoryId="+id);
		return (boolean)subcategory.get(0); 
	}

	@Override
	public List fetchAllSubCategory(int id) {
		System.out.println(checkHasSubCategory(id));
		if (checkHasSubCategory(id)) {
			@SuppressWarnings("unchecked")
			List<CategoryTable> subCategoryList = (List<CategoryTable>) template.find("from CategoryTable c where c.categoryTable.categoryId="+id);
			return  subCategoryList;
			
		} else {
			List<ProductTable> productList = (List<ProductTable>) template.find("from ProductTable p where p.fkCategoryId="+id);
			return productList;
		}
	}

}
