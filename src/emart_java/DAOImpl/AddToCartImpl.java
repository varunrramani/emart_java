package emart_java.DAOImpl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.DAOInterfaces.AddToCartDAO;
import emart_java.Entities.CartTable;
import emart_java.Entities.ProductTable;
import emart_java.Entities.UserInfo;

@Component
public class AddToCartImpl implements AddToCartDAO {
	
	@Autowired
	private HibernateTemplate template;
	
	@Override
	public String addToCart(int productId, UserInfo user, boolean emCurrencyCardCombination, HttpSession session) {
		CartTable cart=new CartTable();
		List<ProductTable> productDetail= (List<ProductTable>) template.find("from ProductTable p where p.productId in (?)",productId );
		ProductTable product=productDetail.get(0);
		if(emCurrencyCardCombination)
		{
			if(user.getEmCardPoints()>=product.getPriceTable().getEmCardPoints())
			{
				double newPoints=user.getEmCardPoints()-product.getPriceTable().getEmCardPoints();
				/*user.setEmCardPoints(newPoints);
				session.setAttribute("userInfo", user);*/
			}
			else
			{
				System.out.println("Insufficient Points");
				return "Insufficient Points";
			}
		}
		cart.setQuantity(1);
		cart.setActive(true);
		cart.setProductTable(product);
		cart.setUserInfo(user);
		cart.setEmCurrencyPointsCombination(emCurrencyCardCombination);
		template.save(cart);
		return "Added to cart";
	}

}