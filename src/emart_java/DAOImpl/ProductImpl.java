package emart_java.DAOImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import emart_java.DAOInterfaces.ProductDAO;
import emart_java.Entities.ProductTable;

@Component
public class ProductImpl implements ProductDAO {

	@Autowired
	private HibernateTemplate template;
	
	@Override
	public List<ProductTable> getDetails(int id) {
		
		List<ProductTable> productList=  (List<ProductTable>) template.find("from ProductTable p  where p.productId="+id);
		return productList;
	}
	
	

}
