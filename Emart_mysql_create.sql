CREATE TABLE `Category_Table` (
	`Category_Id` int(255) NOT NULL AUTO_INCREMENT,
	`Category_Name` varchar(255) NOT NULL,
	`Has_Sub_Category` bool NOT NULL,
	`Fk_Category_Id` int(255) NOT NULL,
	`Image_Url` varchar(255) NOT NULL,
	`Is_Main_Category` bool NOT NULL,
	PRIMARY KEY (`Category_Id`)
);

CREATE TABLE `Price_Table` (
	`Price_Id` int(255) NOT NULL AUTO_INCREMENT,
	`Cost_Price` double NOT NULL,
	`Selling_Price` double NOT NULL,
	`Em_Card_Holders_Price` double NOT NULL,
	`Em_Card_Points` int NOT NULL,
	PRIMARY KEY (`Price_Id`)
);

CREATE TABLE `Product_Table` (
	`Product_Id` int(255) NOT NULL AUTO_INCREMENT,
	`Product_Name` varchar(255) NOT NULL,
	`Fk_Category_Id` int(255) NOT NULL,
	`Product_Quantity` int(255) NOT NULL,
	`Size` int(255) NOT NULL,
	`Price_Id` int(255) NOT NULL,
	`Specification` int(255) NOT NULL,
	`Image_Url` varchar(255) NOT NULL,
	PRIMARY KEY (`Product_Id`)
);

ALTER TABLE `Category_Table` ADD CONSTRAINT `Category_Table_fk0` FOREIGN KEY (`Fk_Category_Id`) REFERENCES `Category_Table`(`Category_Id`);

ALTER TABLE `Product_Table` ADD CONSTRAINT `Product_Table_fk0` FOREIGN KEY (`Fk_Category_Id`) REFERENCES `Category_Table`(`Category_Id`);

ALTER TABLE `Product_Table` ADD CONSTRAINT `Product_Table_fk1` FOREIGN KEY (`Price_Id`) REFERENCES `Price_Table`(`Price_Id`);

