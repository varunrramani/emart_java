
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="js/login_index.js"></script>
 
</head>
<script>
</script>

<body background="login.jpg">
  <div class="login-page">
  	<div class="form1">
  		<h2>Sign In </h2>
  		 <form:form class="login-form"  method="POST" commandName="Login">
    	  	  <form:input path="emailId" placeholder="Email ID"/>
    	  	  <form:input type="password" path="password" placeholder="Password"/>
    		  <button>login</button>
    		  <p>Not registered? <a href="registration.do">Create an account</a></p>
    	</form:form>
	</div>
</div>
</body>
</html>
