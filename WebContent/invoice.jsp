<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Invoice Details</title>
<link rel="stylesheet" href="cart.css">

</head>
<body>

	<div class="tag1">My Invoice</div>

	<div class="header">
		<br> <label for="Customer Name" id="customername">Customer Name:${sessionScope.username}</label> <br>
			 
			 <label for="Bill To" id="billto">Bill to:${sessionScope.username}</label>

		<div id="addrs">
			<label for="address" id="address">Address:${sessionScope.Address_Table.address}</label>
			<br> <label for="locality" id="locality">Locality:${sessionScope.Address_Table.locality}</label>
			<br> <label for="landMark" id="landMark">LandMark:${sessionScope.Address_Table.landMark}</label>
			<br><label for="pinCode" id="pinCode">PinCode:${sessionScope.Address_Table.pincode}</label>  
			<br><label for="mobileNo" id="mobileNo">MobileNo:${sessionScope.Address_Table.mobileno}</label>
		</div>

	</div>


	<div class="container">
		${order }
		<c:forEach items="${order.orderItemTable}" var="item">
			<%-- <div>
				<c:out value="${item }" />
			</div> --%>
			<%-- <div class="nav">
				<center>
					<u>Item No</u>
				</center>
				<c:out value="${item.itemNo}" />

			</div>

			<div class="nav1">
				<center>
					<u>Item Description</u>
				</center>
				<c:out value="${item.itemdescription}" />
			</div>


			<div class="nav2">
				<center>
					<u>Quantity</u>
				</center>
				<center>
				<label for="quantity"><c out value="${item.quantity}" /></label> 
				</center>
			</div>

			<div class="nav3">
				<center>
					<u>List Price</u>
				</center>
				<c:out value="${item.price}" />
			</div>


			<div class="nav4">
				<center>
					<u>Emart Price</u>
				</center>
				<c:out value="${item.emartprice}" />
			</div>

			<div class="nav5">
				<center>
					<u>Amount</u>
				</center>
				<% if(session.getAttribute("cardHolder").equals("no")){ %>
					<c:out value="${item.price}*${item.quantity}" />
					<%}  else {%>
					<c:out value="${item.emartprice}*${item.quantity}" />
					<%} %>

			</div> --%>

		
		</c:forEach>
	</div>

	<div class="footer">
		<center>
			<FORM NAME="form1" METHOD="POST">
				<br> <br> <a href="Invoice.do"><button >Print Invoice</button> 
				
			</FORM>
		</center>
	</div>
</body>
</html>