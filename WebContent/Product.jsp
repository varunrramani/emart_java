<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>AdminProduct</title>
<style>
	.error
	{
		color:red;
	}
</style>
</head>
<body bgcolor="bisque">
<center><h1>Add New Product</h1></center>
<form:form  method="post" commandName="productInfo">
Product Name<form:errors path="productName" cssClass="error" /> <br><br>
<form:input path="productName" placeholder="Enter Product_Name"/><br><br>

Category Id<form:errors path="fkCategoryId" cssClass="error"/> <br><br>
<form:input path="fkCategoryId" placeholder="Enter Category_id"/><br><br>


Product Quantity<form:errors path="productQuantity" cssClass="error"/> <br><br>
<form:input path="productQuantity" placeholder="Enter product_quantity"/><br><br>

Specification<form:errors path="specification" cssClass="error"/> <br><br>
<form:input path="specification" placeholder="Enter specification"/><br><br>

Image Url <form:errors path="imageUrl" cssClass="error"/> <br><br>
<form:input path="imageUrl" placeholder="Enter Image_url"/><br><br>

Cost Price <form:errors path="priceTable.costPrice" cssClass="error"/> <br><br>
<form:input path="priceTable.costPrice" placeholder="Enter cost_price"/><br><br>

Selling Price <form:errors path="priceTable.sellingPrice" cssClass="error"/> <br><br>
<form:input path="priceTable.sellingPrice" placeholder="Enter selling_price"/><br><br>

emCardHolderPrice <form:errors path="priceTable.emCardHoldersPrice" cssClass="error"/> <br><br>
<form:input path="priceTable.emCardHoldersPrice" placeholder="Enter em_cardholder_price"/><br><br>

emCardPoints <form:errors path="priceTable.emCardPoints" cssClass="error"/> <br><br>
<form:input path="priceTable.emCardPoints" placeholder="Enter em_card_points"/><br><br>

<input type=submit value="Submit"/>
</form:form>	
</body>
</html>