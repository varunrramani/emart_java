<%@page import="org.apache.taglibs.standard.tag.common.xml.ForEachTag"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- BootStrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>
<style>
.img{
width:150px;
height:130px;
}
.innerDiv{
border:1px solid black;
float:left;
height:250px;
width:33.33%;
}
</style>
	<body>
<head>
  <meta charset="UTF-8">
  <title>Responsive Image Slider</title>
      <link rel="stylesheet" href="css/slider_style.css">
</head>

<body>
  <div class="slider">
  <div class="slide_viewer">
    <div class="slide_group">
      <div class="slide">
      <a href="SubCategory.do?id=3"><img src="images/slider.png"/></a>
      </div>
      <div class="slide">
      <img src="images/slider3.png"/>

      </div>
      <div class="slide">
      <img src="images/slider5.png"/>
      </div>
      <div class="slide">
            <img src="images/slider2.png"/>
      </div>
      <div class="slide">
            <img src="images/slider1.png"/>
      </div>
            <div class="slide">
            <img src="images/slider.png"/>
      </div>
    </div>
  </div>
</div><!-- End // .slider -->

 
    <script src="js/slider_index.js"></script>
 
		<c:forEach var="i" items="${CategoryList}">
        	 <div class="innerDiv">
				<img src="${i.imageUrl}" height="200px" width="200px">
				<a href="SubCategory.do?id=${i.categoryId}">${i.categoryName}</a>
			 </div>
		</c:forEach>
	</body>
</html>
