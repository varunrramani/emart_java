<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="org.apache.taglibs.standard.tag.common.xml.ForEachTag"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<style>

#productImage{
widht:50%;
height:60%;
float:left;
border:1px dotted black;

}

#productDescription
{
width:60%;
float:left;
height:auto;
border:1px dotted black;
}
</style>

<body>
	<h1>Product Details</h1>
	   <c:forEach var="i" items="${productDetails}">
		<div id="productImage">
			<img src="${i.url}">
		</div>
		<div id="productDescription">
			${i.des}
		</div>
	   <div id="price">
		</div>
		<form action="addToCart.do?id="${i.id}">
			<input type="text" name="id" hidden value="${i.id}">	   
Use Points :<input type="checkbox" name="chk" value="true">				 		
			<input type="submit" id="bt" value="Add To Cart" class="btn btn-default btn-sm" >
        </form>				
      </c:forEach>
</body>
</html>