<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<link rel="stylesheet" href="css/menustyle.css">
<link rel="stylesheet" href="css/login_style.css">
<link rel="stylesheet" href="css/login_style.css">
</head>
<body>
	<nav class="skew-menu">
		<ul>
			<c:forEach items="${CategoryList}" var="item">

				<li><a href="SubCategory.do?id=${item.categoryId}">${item.categoryName}</a></li>

			</c:forEach>
			<li><a href="cart.do">Cart</a></li>
		</ul>
	</nav>
</body>
</html>
