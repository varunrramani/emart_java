<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
      <link rel="stylesheet" href="css/login_style.css">	
</head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
 <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
 <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
 <script>
$(function() {
   $("#datepicker").datepicker();
});
</script>

<style>
	

#f
{
	border:2px solid black;
	width:600px;
	overflow:hidden;
	margin-left:auto;
	margin-right:auto;
	margin-top:50px;
	background-color:white;
	
}
.r1
{
	margin-left:200px;
}
.d
{
	width:200px;
	float:left;
	height:450px;
	overflow:hidden;
}
.v
{
	width:400px;
	float:left;
	height:450px;
	overflow:hidden;
}
span
{
color:black;
}
	</style>
		 
		<script>
		var str2;
		function nameVal()
		{   
			var str1=document.UserForm.UserName.value;
			str1=str1.trim();
			var pattern=/^[A-Za-z0-9_]+$/;
			
			if(pattern.test(str1))
			{
				document.UserForm.UserName.value = str1;
				document.getElementById( "errorUserName" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorUserName" ).innerHTML="Invalid userName";
				return false;
			}	
		}
		
		function passVal1()
		{
			str2= document.UserForm.Pass.value;
			str2=str2.trim()
			var pattern= /^[A-Za-z]{1}[\w]{6,}$/; 
			
		

			if(pattern.test(str2))
			{
				document.UserForm.Pass.value = str2;
				document.getElementById( "errorPass1" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorPass1" ).innerHTML="Invalid Password";
				return false;
			}	
		}
		function passVal2()
		{
			var str3=document.UserForm.rePass.value;
			str3=str3.trim()
			if(str2!=str3)
			{
				document.UserForm.rePass.value = str3;
				document.getElementById( "errorPass2" ).innerHTML="Password should be same";
				return false;
			}
			else
			{
				document.getElementById( "errorPass2" ).innerHTML="";
				return true;
			}
		}
		
		function emailVal()
		{
			var str4=document.UserForm.emailid.value;
			str4=str4.trim()
			var pattern4=/^[a-z0-9]+[_\.]?[a-z0-9]+@[a-z]+\.?[a-z]{2,}$/;
			
			if(pattern4.test(str4))
			{
				document.UserForm.emailid.value = str4;
				document.getElementById( "errorEmail" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorEmail" ).innerHTML="Invalid email address";
				return false;
			}	
		}
		function phonVal()
		{
			var str5=document.UserForm.phoneNo.value;
			str5=str5.trim()
			var pattern5=/^[0-9]{10}$/;;
			if(pattern5.test(str5))
			{
				document.UserForm.phoneNo.value = str5;
				document.getElementById( "errorPhoneNo" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorPhoneNo" ).innerHTML="Invalid phone number";
				return false;
			}	
		}
		
		function ATrim()
		{
			var str6=document.UserForm.Education.value;
			if(str6.trim())
			{
				str6=str6.trim();
				document.UserForm.EducationId.value = str6;
				document.getElementById( "errorEducation" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorEducation" ).innerHTML="Only space is not allowed";
				return false;
			}	
		}
		function BTrim()
		{
			var str7=document.UserForm.Occupation.value;
			if(str7.trim())
			{
				str7=str7.trim();
				document.UserForm.Occupation.value = str7;
				document.getElementById( "errorOccupation" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorOccupation" ).innerHTML="Only space is not allowed";
				return false;
			}	
		}
		
		
		function formSubmit(){

			if((!nameVal())&& (!passVal1())&&(!emailVal())&&(!phoneVal())&& (!pinVal()))
		{
			return false;
		}
		else
		{
			return true;
		}
		}
		</script>
<body>
  <div class="register-page">
  <div class="form">
  <h2>Sign Up</h2>
  <form:form name="UserForm" class="login-form" method="POST" commandName="userinfo">
	  	Username <form:input path="name" type="text" name="UserName" placeholder="Enter User name" onchange="nameVal()"/>
		<span id="errorUserName" ></span><br>
		Password <form:input path="password" type="password" name="Pass" onchange="passVal1()" placeholder="Enter password"  />
		<span id="errorPass1" ></span><br>
		ReEnter Password <input type="Password" name="rePass" onchange="passVal1()" placeholder="Re-Enter password"  />
		<span id="errorPass2" ></span><br>	
		Email ID <form:input path="emailId" type="text" name="emailid" onchange="emailVal()" placeholder="Enter email"  />
		<span id="errorEmail" ></span><br>
		Birth Date <form:input path="birthDate" placeholder="Birth Date" type="date" id="datepicker"/><br>
		Mobile Number <form:input path="mobileNo" type="text" placeholder="Mobile Number" name="phoneNo" onchange="phonVal()" />
		<span id="errorPhoneNo"></span><br>
		Qualification <form:input path="educationalQualification" type="text" name="Education" onchange="ATrim()" placeholder="Education Qulification" />
		<span id="errorEducation" ></span>	<br>	
		Occupation <form:input path="occupation" type="text" name="Occupation" onchange="BTrim()"  placeholder="Occupation" />	
		<span id="errorOccupation" ></span><br>
		Gender <form:input path="gender" Type="text" /><br>
		<%-- EmCard Points<form:input path="emCardPoints" type="text"/><br> --%>
		Is EmCard Holder?<form:input path="isEmCardHolder" Type="text" /><br>
		<button>Submit</button>
	</form:form>
  </div>
  </div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/login_index.js"></script>

</body>
</html>
