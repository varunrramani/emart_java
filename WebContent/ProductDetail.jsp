<%@ page session="false" "java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
     <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
div { 
    display: block;
}
</style>
<body>


<p>This is some text.</p>

<div style="color:#0000FF">
  <h3>This is a heading in a div element</h3>
  <p>This is some text in a div element.</p>
</div>

<form>
<c:forEach var="p"  items="${productdisplay}"> 
			<div class="imagep">
				<img alt="${p.name}" src="${p.imgUrl}" style="height:300px;width:700px;"></img>
			</div>	
			
			<div class="ppname">
			<p><c:out value="${p.ProductName}"/></p>
			<p><c:out value="${p.specification}"/></p>
			  <p><c:out value="${p.PriceTable.sellingPrice }"/></p>
			 </div>	
			
			 
			
			<!-- Add on emcard holder jp page  only if user is privileged 
			<div>
			 <c:out value="${p.PriceTable.emCardHolderPrice}" />
			</div>
			
			<div>
			 <c:out value="${p.PriceTable.emCardPoints}" />
			</div>
			
			-->
			<a href="productDetails.do?id=${p.productId}">${p.productName}</a>
				 	<form action="addToCart.do?id="${p.productId}">
				 	    <input type="text" name="id" hidden value="${p.productId}">
				 		    Use Points :<input type="checkbox" name="chk" value="true">
				 		    <a><input type="submit" id="bt" value="Add To Cart" class="btn btn-default btn-sm" ></button></a>
        			</form>
			
   </c:forEach>
   
</form> 
<p>This is some text.</p>

</body>
</html>
