<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Product Page</title>
<link rel="stylesheet" href="product.css">

</head>
<body>

	<div class="Header">My Product</div>

	<div class="container">
		<c:forEach var="p" items="${productDisplay}">
			<div class="LeftNav">
				<center>
					<u>Product Image</u>
				</center>


				<img  src="${p.imageUrl}"
					style="height: 300px; width: 537px; margin-top: 22Px;"></img>

			</div>


			<div class="RightContainer">


				<div class="ProductName">
					<center>
						<u>Product Name</u>
					</center>
					<c:out value="${p.productName}" />

				</div>

				<div class="Specification">
					<center>
						<u>Specification</u>
					</center>
					<c:out value="${p.specification}" />

				</div>

				<div class="SellingPrice">
					<center>
						<u>SellingPrice</u>
					</center>
					<c:out value="${p.priceTable.sellingPrice }" />
				</div>

				<div class="UsePoint">
					<center>
						<u>UsePoint</u>
					</center>
					<!--<form action="addToCart.do?id="${p.productId}">-->
						<input type="text" id="productId" name="id" hidden value="${p.productId}">
						Use Points :<input type="checkbox" id="usePointCheckbox${p.productId }" name="chk" value="false">
						<!--<a><input type="submit" id="bt" value="Add To Cart"
							class="btn btn-default btn-sm"></a>-->
						</button>
						<button onclick="addToCart(${p.productId})">Add to cart</button>
					<!--</form>-->
		</c:forEach>
	</div>
	</div>
		<script>
	function addToCart(productId) {
			alert(productId);
			alert(document.getElementById("usePointCheckbox" + productId).value);
			$.ajax({
				type : "POST",
				url : "addToCart.do",
				data: {"productId":productId, "chk" : document.getElementById("usePointCheckbox" + productId).value},
				success: function(data)
				{
					if(data=="Login")
						{
						 window.location.href = "login.do";
						}
					else
						{
						setTimeout(function() {
						    $('#messageFromAddToCart').val(data).fadeOut('fast');
						}, 1000); 
						}
					}
				});
		}
		
	</script>
</head>
</html>