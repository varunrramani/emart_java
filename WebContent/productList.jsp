<%@page import="org.apache.taglibs.standard.tag.common.xml.ForEachTag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<!-- BootStrap -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>

<style>
.img {
	padding-top: 10px;
	width: 150px;
	height: 130px;
}

.innerDiv {
	border: 1px solid black;
	float: left;
	height: 250px;
	width: 33.33%;
}

#bt {
	margin-left: 100px;
}

#messageFromAddToCart {
	visibility: hidden;
}
</style>
<body>
	<label id="messageFromAddToCart"></label>
	<h1>Product List</h1>
	<c:forEach var="i" items="${ProductList}">
		<div class="innerDiv">
			<img src="${i.imageUrl}" height="200px" width="200px"> <a
				href="productDetails.do?id=${i.productId}">"${i.productName}"</a>
			<%-- <form action="addToCart.do?id="${i.productId}"> --%>
			<input type="text" id="productId" name="id" hidden
				value="${i.productId}"> Use Points :<input type="checkbox"
				id="usePointCheckbox${i.productId }" name="chk" value="false">
			<a> <!-- <input type="submit" id="bt" value="Add To Cart" class="btn btn-default btn-sm" ></button> -->
				<button onclick="addToCart(${i.productId})">Add to cart</button> <!-- </form> -->
		</div>
	</c:forEach>

	<script>
	function addToCart(productId) {
			alert(productId);
			alert(document.getElementById("usePointCheckbox" + productId).value);
			$.ajax({
				type : "POST",
				url : "addToCart.do",
				data: {"productId":productId, "chk" : document.getElementById("usePointCheckbox" + productId).value},
				success: function(data)
				{
					if(data=="Login")
						{
						 window.location.href = "login.do";
						}
					else
						{
						setTimeout(function() {
						    $('#messageFromAddToCart').val(data).fadeOut('fast');
						}, 1000); 
						}
					}
				});
		}
		
	</script>
</body>
</html>
