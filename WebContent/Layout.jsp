<%@page import="org.apache.taglibs.standard.tag.common.xml.ForEachTag"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Emart-The Online Store</title>
<style>
#content {
    max-width: 1000px;
    margin: auto;
 	background-image:url("${pageContext.request.contextPath}/images/back.jpg");
    padding: 10px;
}
#header{
border: 1px solid black;
border-bottom:none;
float:left;
height:100px;
width:100%;
}
#topmenu{
border: 1px solid black;
float:left;
width:100%;
}
#body{ 
float:left;
width:100%;
background-color: white;
}
#footer{
text-align:center;
border: 1px solid black;
float:left; 
width:100%;
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body id="content" >
	<div id="header" ><tiles:insertAttribute name="header" /></div>
    <div id="topmenu"><tiles:insertAttribute name="topmenu"/></div> 
    <div id="body"><tiles:insertAttribute name="body" /></div>
    <div id="footer"><tiles:insertAttribute name="footer" /></div>
</body>
</html>