<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="${pageContext.request.contextPath}/js/jquery-1.8.2.js"></script>
<script src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Cart Page</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/cart.css">


<script>
	$(document).ready(function() {
		/* $('#remove').click(function(event) {
			 alert("remove called");
		    var remove = $("#remove").val();
		   
		    //var select = $('#movie');
		    deleteItem(remove);
		      
		}); */

		function deleteItem(cartId) {
			$.ajax({
				type : "DELETE",
				url : "cart/" + cartId,
				success : function(data) {
					console.log("SUCCESS: ", data);
					//$('#remove cartId').remove();

				}
			});
		}
	});
</script>




</head>
<body>

	<div class="tag1">My Cart</div>

	<div class="header">
		<br> <label for="Customer Name" id="customername">Customer
			Name:${sessionScope.username}</label> <br> <label for="Bill To"
			id="billto">Bill to:</label>

		<div id="addrs">
			<label for="address" id="address">Address:</label> <br> <label
				for="locality" id="locality">Locality:</label> <br> <label
				for="landMark" id="landMark">LandMark:</label> <br> <label
				for="pinCode" id="pinCode">PinCode:</label> <br> <label
				for="mobileNo" id="mobileNo">Mobile No:</label>
		</div>

	</div>


	<div class="container">
		<!-- <script id="my-template" type="text/x-jsrender"> -->
		
		<c:forEach items="${listCartTable}" var="item">
			<%-- <div>
				<c:out value="${item }" />
			</div> --%>
			<div class="ItemNo">
				<center>
					<u>Item No</u>
				</center>
				<c:out value="${item.cartId}" />
			</div>

			<div class="ItemDescription">
				<center>
					<u>Item Description</u>
				</center>
				<c:out value="${item.productTable.productName}" />
			</div>


			<div class="Quantity">
				<center>
					<u>Quantity</u>
				</center>
				<br>


				<center>
					<form action="updateCartItem.do" method="POST">
						<label for="quantity">Qty: </label> 
						<input type="hidden" value="${item.cartId }" id="cartId" name="cartId" /> 
						<input min="1" type="number" id="quantity" name="quantity" value="${item.quantity }" />
						<input type="submit" value="Update" />
					</form>
				</center>
			</div>

			<div class="ListPrice">
				<center>
					<u>List Price</u>
				</center>
				<c:out value="${item.productTable.priceTable.sellingPrice}" />

			</div>

			<div class="EmartPrice">
				<center>
					<u>Emart Price</u>
				</center>
				<c:out value="${item.productTable.priceTable.emCardHoldersPrice}" />
			</div>

			<div class="Amount">
				<center>
					<u>Amount</u>
				</center>
			</div>

			<div class="Remove">
				<center>
					<u>Remove?</u><br> <br>
					<form action="deleteCartItem.do" method="POST">
						<input type="hidden" value="${item.cartId }" name="cartId" /> 
						<input type="submit" value="Remove" />
					</form>
				</center>
			</div>
		</c:forEach>
		<!-- </script> -->
	</div>

	<div class="footer">
		<center>
			<FORM NAME="form1" action="createOrder.do" METHOD="POST">
				<input type="submit" value="Place Order" />
			</FORM>
		</center>
	</div>

</body>
</html>