<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" href="css/login_style.css">
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css" />
  

		<title>
			Login
		</title>
		<script>
		var str2;
		
		
		
		function phonVal()
		{
			var str1=document.Addressform.PhoneNo.value;
			str1=str1.trim()
			var pattern1=/^[0-9]{10}$/;;
			if(pattern1.test(str1))
			{
				document.Addressform.PhoneNo.value = str1;
				document.getElementById( "errorPhoneNo" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorPhoneNo" ).innerHTML="Invalid phone number";
				return false;
			}	
		}
		function pinVal()
		{
			var str2=document.Addressform.PinCode.value;
			str2=str2.trim()
			var pattern2=/^[0-9]{6}$/;
			if(pattern2.test(str2))
			{
				document.Addressform.PinCode.value = str2;
				document.getElementById( "errorPinCode" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorPinCode" ).innerHTML="Invalid pin code number";
				return false;
			}	
		}
		function formSubmit(){

		if((!emailVal())&&(!pinVal()))
		{
			return false;
		}
		else
		{
			return true;
		}
		}
		function ATrim()
		{
			var str3=document.Addressform.city.value;
			if(str3.trim())
			{
				str3=str3.trim();
				document.Addressform.city.value = str3;
				document.getElementById( "errorCity" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorCity" ).innerHTML="Only space is not allowed";
				return false;
			}	
		}
		function BTrim()
		{
					
			var str4=document.Addressform.flat.value;
			if(str4.trim())
			{
				str4=str4.trim();
				document.Addressform.flat.value = str4;
				document.getElementById( "errorflat" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorflat" ).innerHTML="Only space is not allowed";
				return false;
			}	
		}
		function CTrim()
		{
			
			
			var str5=document.Addressform.colony.value;
			if(str5.trim())
			{
				str5=str5.trim();
				document.Addressform.colony.value = str5;
				document.getElementById( "errorcolony" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorcolony" ).innerHTML="Only space is not allowed";
				return false;
			}	
		}
		function DTrim()
		{
			
			
			var str6=document.Addressform.landmark.value;
			
			if(str6.trim())
			{
				str6=str6.trim();
				document.Addressform.landmark.value = str6;
				document.getElementById( "errorlandmark" ).innerHTML="";
				return true;
			}
			else
			{
				document.getElementById( "errorlandmark" ).innerHTML="Only space is not allowed";
				return false;
			}	
			
			
		}
		
		
		
		</script>
		


</head>
<body>
	
	  <div class="register-page">
  <div class="form">
  <h2>Add Address </h2>
		<form:form id="f" name="registration_form" method="post" onsubmit="return formSubmit()" commandName="address"> <br/>
		Mobile No: <form:input path="mobileNo" type="number"  name="PhoneNo" onchange="phonVal()" placeholder="Enter phone no."/>
		<span id="errorPhoneNo"></span><br>
		City :<form:input path="city" type="text" name="city" onchange="ATrim()" placeholder="City"/>	
		<span id="errorCity"></span><br>	
		Pincode :<form:input path="pinCode" type="number" name="PinCode" onchange="pinVal()"  placeholder="Enter pin code"/>
		<span id="errorPinCode"></span><br>
		Address :<form:input path="address" type="text" name="flat" onchange="BTrim()" placeholder="Flat"/>
		<span id="errorflat"></span><br>
		Colony :<form:input path="locality" type="text" name="colony" onchange="CTrim()" placeholder="ColonyLandmark"/>
		<span id="errorcolony"></span><br>
		Landmark :<form:input path="landMark" type="text" name="landmark" onchange="DTrim()" placeholder="Landmark"/>
		<span id="errorlandmark"></span><br>			
		<button>Submit</button>
		</form:form>
		</div>
  </div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/login_index.js"></script>
	</body>
</html>
